console.log('mi primera ejecución con Node');
// Vamos a crear nuestra primera función!
function sum(num1, num2) {
	return num1 + num2;
}
// Ya tenemos declarada nuestra función para sumar dos números

var result = sum(2, 2); // Aqui estamos invocando a la función (ejecutándola)
console.log(result); // Y aquí vamos a imprimir el resultado
